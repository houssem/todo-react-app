import React, { useState } from "react";
import axios from "axios";
import Header from "../components/Header";
import Footer from "../components/Footer";
import RightBar from "../components/RightBar";
import { Redirect, Route } from "react-router";

export default function TodoForm({ token }) {
  const [inputs, setInputs] = useState({
    name: "",
    description: "",
  });
  const BASE_URL = "http://localhost:8282/api/task/add";
  const handleInputsChange = (e) => {
    const target = e.target;
    const value = target.value;
    const name = target.name;

    setInputs((values) => ({
      ...values,
      [name]: value,
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    axios
      .post(BASE_URL, inputs, { headers: { Authorization: `Bearer ${token}` } })
      .then(() => {
        window.location = "/Todo/index";
      })
      .catch((error) => {
        console.log(error);
      });
  };
  return (
    <div className="wrapper">
      <Header />
      <RightBar />
      <div className="content-wrapper">
        <section className="content">
          <div className="row">
            <div className="col-12">
              <div className="card card-primary">
                <div className="card-header">
                  <h3 className="card-title">Add Task</h3>
                </div>
                {/* /.card-header */}
                {/* form start */}
                <form role="form" onSubmit={handleSubmit}>
                  <div className="card-body">
                    <div className="form-group">
                      <label htmlFor="description">Task Name</label>
                      <input
                        type="text"
                        className="form-control"
                        id="exampleInputEmail1"
                        placeholder="Task Name"
                        name="name"
                        onChange={handleInputsChange}
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="description">Task Decsription</label>
                      <input
                        type="text"
                        className="form-control"
                        id="decsription"
                        placeholder="Description"
                        name="description"
                        onChange={handleInputsChange}
                      />
                    </div>
                  </div>
                  {/* /.card-body */}
                  <div className="card-footer">
                    <button type="submit" className="btn btn-primary">
                      Save
                    </button>
                    <a href="/" className="btn btn-danger">
                      Cancel
                    </a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </section>
      </div>
      <Footer />
    </div>
  );
}
