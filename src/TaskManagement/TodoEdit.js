import React, { useEffect } from "react";
import axios from "axios";
import Header from "../components/Header";
import Footer from "../components/Footer";
import RightBar from "../components/RightBar";
import { useParams } from "react-router-dom";
import UnauthorizedComponent from "../components/UnauthorizedComponent";
import ReactDOM from "react-dom";

export default function TodoEdit({ token }) {
  const [task, setTask] = React.useState({});
  let { task_id } = useParams();
  const BASE_URL = `http://localhost:8282/api/task`;

  const getTaskById = async () => {
    const result = await axios
      .get(`${BASE_URL}/showForEdit/${task_id}`, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => {
        setTask(response.data);
      })
      .catch((error) => {
        ReactDOM.render(
          <UnauthorizedComponent response={error.response.data} />,
          document.getElementById("root")
        );
      });
  };

  useEffect(() => {
    getTaskById();
  }, []);
  const handleInputsChange = (e) => {
    const target = e.target;
    const value = target.value;
    const name = target.name;

    setTask((values) => ({
      ...values,
      [name]: value,
    }));
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    const result = axios.put(`${BASE_URL}/edit/${task_id}`, task, {
      headers: { Authorization: `Bearer ${token}` },
    });
    window.location = "/Todo/index";
  };
  return (
    <div className="wrapper">
      <Header />
      <RightBar />
      <div className="content-wrapper">
        <section className="content">
          <div className="row">
            <div className="col-12">
              <div className="card card-primary">
                <div className="card-header">
                  <h3 className="card-title">Add Task</h3>
                </div>
                {/* /.card-header */}
                {/* form start */}
                <form role="form" onSubmit={handleSubmit}>
                  <div className="card-body">
                    <div className="form-group">
                      <label htmlFor="description">Task Name</label>
                      <input
                        type="text"
                        className="form-control"
                        id="exampleInputEmail1"
                        placeholder="Task Name"
                        name="name"
                        value={task.name}
                        onChange={handleInputsChange}
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="description">Task Decsription</label>
                      <input
                        type="text"
                        className="form-control"
                        id="decsription"
                        placeholder="Description"
                        name="description"
                        value={task.description}
                        onChange={handleInputsChange}
                      />
                    </div>
                  </div>
                  {/* /.card-body */}
                  <div className="card-footer">
                    <button type="submit" className="btn btn-primary">
                      Save
                    </button>
                    <a href="/" className="btn btn-danger">
                      Cancel
                    </a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </section>
      </div>
      <Footer />
    </div>
  );
}
