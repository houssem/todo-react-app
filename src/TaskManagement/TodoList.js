import React, { useState } from "react";
import axios from "axios";
import Header from "../components/Header";
import Footer from "../components/Footer";
import RightBar from "../components/RightBar";
import swal from "sweetalert";
import ReactDOM from "react-dom";
import UnauthorizedComponent from "../components/UnauthorizedComponent";

export default function TodoList({ token }) {
  const [tasks, setTask] = useState([]);
  const BASE_URL = "http://localhost:8282/api/tasks";

  const getTasks = async () => {
    const result = await axios.get(BASE_URL, {
      headers: { Authorization: `Bearer ${token}` },
    });
    setTask(result.data);
  };

  const handleOnClickDeleteTask = (e) => {
    let task_id = e.target.id;
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this task!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        axios
          .delete(`http://localhost:8282/api/task/delete/${task_id}`, {
            headers: { Authorization: `Bearer ${token}` },
          })
          .then((response) => {
            setTask(tasks.filter((item) => item.id !== task_id));
            swal("Your task has been deleted!", {
              icon: "success",
            }).then(() => {
              window.location = "/Todo/index";
            });
          })
          .catch((error) => {
            ReactDOM.render(
              <UnauthorizedComponent response={error.response.data} />,
              document.getElementById("root")
            );
          });
      } else {
        swal("Your task  is safe!", {
          icon: "info",
        });
      }
    });
  };

  const handleOnClickCompletedTask = (e) => {
    console.log("completed");
    let task_id = e.target.id;
    swal({
      title: "Are you sure?",
      text: "Change Task Status to Completed!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((completed) => {
      if (completed) {
        axios
          .get(`http://localhost:8282/api/task/completed/${task_id}`, {
            headers: { Authorization: `Bearer ${token}` },
          })
          .then((response) => {
            swal(response.data.message, {
              icon: response.data.error == false ? "success" : "warning",
            }).then(() => {
              window.location = "/Todo/index";
            });
          })
          .catch((error) => {
            ReactDOM.render(
              <UnauthorizedComponent response={error.response.data} />,
              document.getElementById("root")
            );
          });
      } else {
        swal("Your task  is safe!", {
          icon: "info",
        });
      }
    });
  };

  const handleOnClickNotCompletedTask = (e) => {
    console.log("make task not completed");
    let task_id = e.target.id;
    swal({
      title: "Are you sure?",
      text: "Change Task Status to Not Completed!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((notCompleted) => {
      if (notCompleted) {
        axios
          .get(`http://localhost:8282/api/task/notcompleted/${task_id}`, {
            headers: { Authorization: `Bearer ${token}` },
          })
          .then((response) => {
            swal(response.data.message, {
              icon: response.data.error == false ? "success" : "warning",
            }).then(() => {
              window.location = "/Todo/index";
            });
          })
          .catch((error) => {
            ReactDOM.render(
              <UnauthorizedComponent response={error.response.data} />,
              document.getElementById("root")
            );
          });
      } else {
        swal("Your task  is safe!", {
          icon: "info",
        });
      }
    });
  };
  React.useEffect(() => {
    getTasks();
  }, []);
  return (
    <div className="wrapper">
      <Header />
      <RightBar />
      <div className="content-wrapper">
        <section className="content">
          <div className="row">
            <div className="col-12">
              <div className="card">
                <div className="card-header">
                  <h3 className="card-title">
                    <a href="/Todo/add" className="btn btn-primary">
                      Add
                    </a>
                  </h3>
                  <div className="card-tools">
                    <div
                      className="input-group input-group-sm"
                      style={{ width: 150 }}
                    >
                      <input
                        type="text"
                        name="table_search"
                        className="form-control float-right"
                        placeholder="Search"
                      />
                      <div className="input-group-append">
                        <button type="submit" className="btn btn-default">
                          <i className="fas fa-search" />
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
                {/* /.card-header */}
                <div
                  className="card-body table-responsive p-0"
                  style={{ height: 300 }}
                >
                  <table className="table table-head-fixed text-nowrap">
                    <thead>
                      <tr>
                        <th>*</th>
                        <th>Task Name</th>
                        <th>Description</th>
                        <th>Status</th>
                        <th>Updated At</th>
                        <th>Created At</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      {tasks.map((task, key) => (
                        <tr key={key.toString()}>
                          <td>{key + 1}</td>
                          <td>{task.name}</td>
                          <td>{task.description}</td>
                          <td>
                            <span
                              className={
                                task.status == "not_complete"
                                  ? "badge badge-danger"
                                  : "badge badge-success"
                              }
                            >
                              {task.status.replace("_", " ")}
                            </span>
                          </td>
                          <td>{task.updated_at}</td>
                          <td>{task.created_at}</td>
                          <td>
                            <a
                              href={`/Todo/edit/${task.id}`}
                              className="btn btn-primary"
                            >
                              Edit
                            </a>
                            <a
                              href="#"
                              className="btn btn-danger"
                              id={task.id}
                              onClick={handleOnClickDeleteTask}
                            >
                              Delete
                            </a>
                            <a
                              href="#"
                              className={
                                task.status == "complete"
                                  ? "btn btn-danger"
                                  : "btn btn-success"
                              }
                              id={task.id}
                              onClick={
                                task.status == "complete"
                                  ? handleOnClickNotCompletedTask
                                  : handleOnClickCompletedTask
                              }
                            >
                              {task.status == "complete"
                                ? "Not Finished"
                                : "Finiched"}
                            </a>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
                {/* /.card-body */}
              </div>
              {/* /.card */}
            </div>
          </div>
        </section>
      </div>
      <Footer />
    </div>
  );
}
