import React, { useState } from "react";
import "./App.css";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import TodoList from "./TaskManagement/TodoList";
import TodoAdd from "./TaskManagement/TodoAdd";
import Login from "./components/Login";
import Home from "./components/Home";
import TodoEdit from "./TaskManagement/TodoEdit";

export default function App() {
  const getToken = () => {
    return localStorage.getItem("token");
  };
  const [token, setToken] = useState(getToken);
  if (!token) {
    return <Login setToken={setToken} />;
  }

  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Home token={token} />
        </Route>
        <Route path="/Todo/index">
          <TodoList token={token} />
        </Route>{" "}
        <Route path="/Todo/add">
          <TodoAdd token={token} />
        </Route>
        <Route path="/Todo/Edit/:task_id">
          <TodoEdit token={token} />
        </Route>
      </Switch>
    </Router>
  );
}
