import React, { useState } from "react";
import axios from "axios";
import { Redirect } from "react-router-dom";
import Home from "../components/Home";
import ReactDOM from "react-dom";

export default function Login({ setToken }) {
  const [inputs, setInputs] = useState({
    password: "",
    email: "",
  });

  const handleInputsChange = (e) => {
    const target = e.target;
    const value = target.value;
    const name = target.name;

    setInputs((values) => ({
      ...values,
      [name]: value,
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const BASE_URL = "http://localhost:8282/api/login";
    console.log(inputs);
    axios
      .post(BASE_URL, inputs)
      .then((result) => {
        setToken(result.data.token);
        setInputs({ password: "", email: "" });
        localStorage.setItem("token", result.data.token);
        ReactDOM.render(<Home />, document.getElementById("root"));
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div className="card card-primary">
      <div className="card-header">
        <h3 className="card-title">Login</h3>
      </div>
      {/* /.card-header */}
      {/* form start */}
      <form role="form" onSubmit={handleSubmit} method="POST">
        <div className="card-body">
          <div className="form-group">
            <label htmlFor="email">Email</label>
            <input
              type="email"
              className="form-control"
              id="email"
              placeholder="Email"
              name="email"
              onChange={handleInputsChange}
            />
          </div>
          <div className="form-group">
            <label htmlFor="password">Task Name</label>
            <input
              type="password"
              className="form-control"
              id="exampleInputEmail1"
              placeholder="password"
              name="password"
              onChange={handleInputsChange}
            />
          </div>
        </div>
        {/* /.card-body */}
        <div className="card-footer">
          <button type="submit" className="btn btn-primary">
            Save
          </button>
          <a href="/" className="btn btn-danger">
            Cancel
          </a>
        </div>
      </form>
    </div>
  );
}
