// eslint-disable-next-line
import React from "react";
import swal from "sweetalert";

export default function UnauthorizedComponent(response) {
  console.log(response.response.error);
  setTimeout(() => {
    swal({
      title: "Ops!",
      text: response.response.message,
      icon: "error",
    }).then(function () {
      window.location = "/Todo/index";
    });
  }, 1500);

  return null;
}
